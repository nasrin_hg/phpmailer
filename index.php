<?php
 include_once "init.php";
 if(isset($_SESSION['loggedin'])){
		header("Location: profile.php");
 }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ثبت نام در سایت</title>
	<link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>

	<div id="users">
		<form action="proccess/do-register.php" method="post">
			<input type="text" name="display-name" class="input" placeholder="نام شما ..."><br>
			<input type="text" name="email" class="input" placeholder="ایمیل شما ..."><br>
			<input type="password" name="password" class="input" placeholder="رمز شما ..."><br>
			<input type="password" name="password-conf" class="input" placeholder="تکرار رمز شما ..."><br>
			<input type="text" name="Subject" class="input" placeholder="موضوع"><br>
			<textarea cols="35" rows="6" name="Message" placeholder="متن پیام"></textarea>
			<button type="submit">ثبت نام</button>
		</form>
	</div>
</body>
</html>